package org.tio.webpack.compress;

/**
 * @author tanyaowu 
 * 2017年11月20日 上午11:03:45
 */
public interface ResCompressor {

	/**
	 * 
	 * @param filePath 
	 * @param srcContent 原内容
	 * @return 压缩后的内容
	 * @author tanyaowu
	 */
	public String compress(String filePath, String srcContent);
	
	String CHARSET = "utf-8";
	
	String DOC = "\r\n1、抱歉让您看到的代码全是压缩的"
			+ "\r\n2、小本经营请勿压测"
			+ "\r\n3、本站使用了tio-http-server, tio-websocket-server, tio-webpack等技术"
			+ "\r\n4、如果本站把您的IP拉黑，冤有头债有主，请去码云找tio ： https://gitee.com/tywo45/t-io"
			+ "\r\n5、如果您对本站有想法，想提升本站逼格，请发邮件至：tywo45@163.com"
			+ "\r\n";
}
